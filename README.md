#第一财经JJ打鱼租号客服bna
#### 介绍
JJ打鱼租号客服【溦:844825】，JJ打鱼租号客服【溦:844825】，　　从杭州到如皋需要一天时间，带着焦急的心情，又进行了长途跋涉，傍晚时候到达如皋，如皋地处海边，是一个不大的城镇。站在旅馆的窗前眺望，风景别致。次日，药厂还没开门，我已早早地在门外等候着。大约八点的样子，药厂的大门开了。在药厂的会计室，当我将那一万块钱递到了药厂财务人员手中，我的心里踏实了，总算完成了一件神圣的任务。当我怀揣着药品回到家时，医生立即给父亲通过静脉输入此药，可是父亲还没用上四支药剂，没过几日，他老人家却永远地离开了我们。
　　吃呱呱最讲究的是佐料。当一碗满碗流红、油辣飘香的呱呱再调配上蒜汁、芥末、芝麻酱、盐、醋等十多种鲜味佐料后，不等你去吃，就是看上两眼都馋延欲滴了。
　　　　　　　　——《古诗源?击壤歌》　　　与《敕勒歌》《大风歌》不同，我宁愿这首《击壤歌》是透过《古诗源》传递给我们的一种劳作的快乐，我更愿相信它不是一位老农而是一位闲散的智者吟唱给周遭小儿的无调之乐。因为，有一点，它是真正从丰盈的田野边流传出来的。　　站在和风丽日下大歌大唱的农人，淳朴得就象山谷中涌来的鲜润空气。他遥望着天边即将落下的夕阳，又回头看着身旁疲惫的耕牛，歌声随口而出，且优美之至。那是一种自由的表述，也是一种物我全无的倾诉。自给自足，素朴从容，安逸和睦，悠然自得。在这种物质与身心的极度满足和极度愉娱中，要放声吟唱再也没有什么比身旁的土壤更随心所欲的乐器了。天是帷幕地是舞台。在秋野的点缀下，与其说老农在欢快地歌唱，毋宁说是丰收之后土地之精灵在无拘无束地舞蹈。对土地的亲近，对土地的热爱，谁能比得上与之相依为命的农人呢？或许，我们可以把这种歌唱想象成一种仪式，一种老农对天地之感恩谢德的仪式。　　帝王的权力和威风被大山之外的奴才们捧着，奉着。为了一己之利的小人也在攘攘着，熙熙着。他们争得头破血流，甚至妻离子散。但是，所有这些和每天面对土地的农人有什么关系呢？野狼之争和蟋蟀之争又有什么不同呢？农人依然在唱着，几千年后的我们捧着纸书依然在听着。诗歌就这样诞生了，文化的古源头就这样被发现了。它们远离权力，远离纷争，远离俗不可耐。也就是这样，在千年之上它将清洁地存在着，并将永远地存在着。　　劳动并快乐着。劳动就快乐着。“这时他凭临美的汪洋大海，凝神观照，心中起无限欣喜，于是孕育无量数的优美崇高的道理，得到丰富的哲学收获。”“那些景象全是完整的，单纯的，静穆的，欢喜的，沉浸在最纯洁的光辉之中让我们凝视。”（《柏拉图文艺对话集?会饮篇》）二千四百多年前的柏拉图以辉煌璨烂的词句观照着人生的最高理想和人性的最高境界，而这种理想和境界已被早他而生的中国农人实现并倡导着。中西方的智者通过对大自然的领悟和谐地汇通在一起，真让我们感叹“万物有灵”！在这里，古希腊庆祝的酬神的“会饮”之乐与农夫的击壤而歌又有什么不同呢？　　大地是坚实的，无垠的。但是今天，当长江黄河源头之土已呈沙漠化，丰富的生命之源面临枯竭时，我们还能找回昔日倾听农人放歌的土壤吗？当人们都热衷于追逐权力倾情名利时，我们还能分辨出农人所唾弃所蔑视的“帝力”吗？　　苏格拉底曾说，“象愤怒，恐惧，忧郁，哀伤……之类情感，你是否把它们看成心灵所特有的痛感呢？”然而，在一些人眼里，这些情感难道不是又充满了极大的快感吗？农人的击壤之歌由此而变得更加沉重，更加苦涩，甚至已无法吟唱。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/